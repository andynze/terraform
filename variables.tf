variable "infrastructure" {
  type        = string
  default     = ""
  description = "Tag name for capturing infrastructure pattern name."
}